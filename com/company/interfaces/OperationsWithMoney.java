package com.company.interfaces;

public interface OperationsWithMoney {
    int MIN_LIMIT = 10;
    int DEPOSIT_MIN_TIME = 12;
    float iterateMoney(float[] values);
}
