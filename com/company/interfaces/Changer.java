package com.company.interfaces;

public interface Changer {
    float MIN_LIMIT_OF_CHANGE = 5;
    double change(float value, String from, String on);
    boolean hasCurrency(String currency);
}
