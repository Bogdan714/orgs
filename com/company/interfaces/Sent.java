package com.company.interfaces;

public interface Sent {
    float MIN_PACKAGE = 10;
    float sendAndGet(float pkg);
}
