package com.company.additional;

import com.company.interfaces.Changer;
import com.company.interfaces.OperationsWithMoney;
import com.company.interfaces.Sent;
import com.company.models.Org;

import java.util.List;

public class Finance {
    private List<Org> organizations;

    public Finance() {
        this.organizations = Generator.generateOrganizations(new int[]{5, 5, 5, 5, 5, 5, 5});
    }

    public double exchange(float money, String from, String to) {
        double bestResult = -1;
        int index = 0;
        for (int i = 0; i < organizations.size(); i++) {
            Org organization = organizations.get(i);
            if (organization instanceof Changer) {
                if (((Changer) organization).change(money, from, to) > bestResult) {
                    bestResult = ((Changer) organization).change(money, from, to);
                    index = i;
                }
            }
        }
        System.out.println(organizations.get(index));
        return bestResult;
    }

    public double takeCredit(float money, float time) {
        float bestResult = Float.MAX_VALUE;
        int index = 0;
        for (int i = 0; i < organizations.size(); i++) {
            Org organization = organizations.get(i);
            if (organization instanceof OperationsWithMoney) {
                float temp = ((OperationsWithMoney) organization).iterateMoney(new float[]{money, time, -1});
                if (temp < bestResult && temp > 0) {
                    bestResult = temp;
                    index = i;
                }
            }
        }
        System.out.println(organizations.get(index));
        return bestResult;
    }

    public double investDeposit(float money, float time) {
        float bestResult = 0;
        int index = 0;
        for (int i = 0; i < organizations.size(); i++) {
            Org organization = organizations.get(i);
            if (organization instanceof OperationsWithMoney) {
                if (((OperationsWithMoney) organization).iterateMoney(new float[]{money, time, 1}) > bestResult) {
                    bestResult = ((OperationsWithMoney) organization).iterateMoney(new float[]{money, time, 1});
                    index = i;
                }
            }
        }
        System.out.println(organizations.get(index));
        return bestResult;
    }

    public double sendPkg(float pkg) {
        float bestResult = 0;
        int index = 0;
        for (int i = 0; i < organizations.size(); i++) {
            Org organization = organizations.get(i);
            if (organization instanceof Sent) {
                if (((Sent) organization).sendAndGet(pkg) > bestResult) {
                    bestResult = ((Sent) organization).sendAndGet(pkg);
                    index = i;
                }
            }
        }
        System.out.println(organizations.get(index));
        return bestResult;
    }


}

