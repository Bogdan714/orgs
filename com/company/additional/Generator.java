package com.company.additional;

import com.company.models.Bank;
import com.company.models.CreditOrg;
import com.company.models.Currency;
import com.company.models.DepositOrg;
import com.company.models.Exchanger;
import com.company.models.Org;
import com.company.models.Sender;

import java.util.ArrayList;
import java.util.List;

public class Generator {
    public static List<Bank> generateBanks(int number) {
        List<Bank> banks = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            banks.add(new Bank("Bank " + i, "Pushkinska str. " + i,
                    0.01f, 0.25f + ((float) Math.random()/10), 0.15f + ((float) Math.random()/10),
                    0.05f, 200_000, 12_000));
        }
        return banks;
    }

    public static List<Sender> generateSenders(int number) {
        List<Sender> senders = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            senders.add(new Sender("Post office " + i, "River str. " + i, 0.02f));
        }
        return senders;
    }


    public static List<Exchanger> generateExchangers(int number) {
        List<Exchanger> exchangers = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            exchangers.add(new Exchanger("Exchanger " + i, "Sumska str. " + i, 0, 5000));
        }
        return exchangers;
    }

    public static List<Currency> generateCurrencies(float grow) {
        List<Currency> currencies = new ArrayList<>();
        String[] names = {"USD", "EUR", "RUB", "UAH"};
        double[] costs = {26.1, 29.2, 0.4, 1};
        for (int i = 0; i < names.length-1; i++) {
            currencies.add(new Currency(names[i], costs[i] + (Math.random() + grow)));
        }
        currencies.add(new Currency(names[names.length-1], costs[costs.length-1]));
        return currencies;
    }

    public static List<CreditOrg> generateCreditCafes(int number) {
        List<CreditOrg> creditCafes = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            creditCafes.add(new CreditOrg("Cafe " + i, "Marshala Zhukova str. " + i, 4000, 2.0f));
        }
        return creditCafes;
    }

    public static List<CreditOrg> generateCreditUnions(int number) {
        List<CreditOrg> creditUnions = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            creditUnions.add(new CreditOrg("Union of creditors " + i, "Marshala Bazhanova str. " + i, 100000, 0.2f));
        }
        return creditUnions;
    }

    public static List<CreditOrg> generateLombards(int number) {
        List<CreditOrg> lombards = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            lombards.add(new CreditOrg("lombard " + i, "Marshala Koneva str. " + i, 50000, 0.4f));
        }
        return lombards;
    }

    public static List<DepositOrg> generatePIFs(int number) {
        List<DepositOrg> PIFs = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            PIFs.add(new DepositOrg("PIF " + i, "Girshmana ", Integer.MAX_VALUE, 0.15f, 12));
        }
        return PIFs;
    }

    public static List<Org> generateOrganizations(int[] orgsVal) {

        List<Org> organizations = new ArrayList<>();

        organizations.addAll(Generator.generateBanks(orgsVal[0]));
        organizations.addAll(Generator.generateCreditCafes(orgsVal[1]));
        organizations.addAll(Generator.generateCreditUnions(orgsVal[2]));
        organizations.addAll(Generator.generateExchangers(orgsVal[3]));
        organizations.addAll(Generator.generateLombards(orgsVal[4]));
        organizations.addAll(Generator.generatePIFs(orgsVal[5]));
        organizations.addAll(Generator.generateSenders(orgsVal[6]));

        return organizations;
    }

    public static List<String> getCurrList(){
        List<String> currencies =  new ArrayList<>();
        currencies.add("USD");
        currencies.add("EUR");
        currencies.add("RUB");
        currencies.add("UAH");
        return currencies;
    }
}
