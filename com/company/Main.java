package com.company;

import com.company.additional.Finance;
import com.company.additional.Generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        Finance market = new Finance();
        int operationNum;
        do {
            System.out.println("choose option: \n" +
                    "1 - credit \n" +
                    "2 - deposit \n" +
                    "3 - change \n" +
                    "4 - send \n");
            operationNum = Integer.parseInt(scan.nextLine());
        } while (operationNum > 4 || operationNum < 1);
        switch (operationNum) {
            case 1:
                System.out.print("Enter the amount of money: ");
                float amount = Float.parseFloat(scan.nextLine());
                System.out.print("Enter the time in month: ");
                float time = Float.parseFloat(scan.nextLine());
                double result = market.takeCredit(amount, time);
                if (result < 0) {
                    System.out.println("something went wrong");
                } else {
                    System.out.println("your debt now: " + result);
                }
                break;
            case 2:
                System.out.print("Enter the amount of money: ");
                amount = Float.parseFloat(scan.nextLine());
                System.out.print("Enter the time in month: ");
                time = Float.parseFloat(scan.nextLine());
                result = market.investDeposit(amount, time);
                if (result < 0) {
                    System.out.println("something went wrong");
                } else {
                    System.out.println("their debt now: " + result);
                }
                break;
            case 3:
                System.out.print("Enter the amount of money: ");
                amount = Float.parseFloat(scan.nextLine());
                String curr1, curr2;
                List<String> currencies = Generator.getCurrList();

                do {
                    System.out.println("Enter the name of currency you have: ");
                    System.out.println(currencies);
                    curr1 = scan.nextLine();
                }while (!currencies.contains(curr1.toUpperCase()));
                do {
                    System.out.println("Enter the name of the currency you want to exchange on: ");
                    System.out.println(currencies);
                    curr2 = scan.nextLine();
                }while (!currencies.contains(curr1.toUpperCase()));
                result = market.exchange(amount, curr1, curr2);
                if (result < 0) {
                    System.out.println("something went wrong");
                } else {

                    System.out.println(String.format("%s%.2f %s","your money now: ",result,curr2.toUpperCase()));
                }
                break;
            case 4:
                System.out.print("Enter the amount of money: ");
                amount = Float.parseFloat(scan.nextLine());
                result = market.sendPkg(amount);
                if (result < 0) {
                    System.out.println("something went wrong");
                } else {
                    System.out.println(String.format("%s%.2f","money after sending: ",result));
                }
                break;
        }
    }
}
