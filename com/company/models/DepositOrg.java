package com.company.models;

import com.company.models.CreditOrg;

public class DepositOrg extends CreditOrg {
    int minTime;

    public DepositOrg(String name, String address, int maxLimit, float percent, int minTime) {
        super(name, address, maxLimit, percent);
        this.minTime = minTime;
    }

    @Override
    public float iterateMoney(float[] values) {
        if (values.length < 3 || values[0] < MIN_LIMIT || values[2] < 0) {
            return -1;
        } else if (values[1] >= DEPOSIT_MIN_TIME) {
            for (int i = 0; i < values[1]; i++){
                values[0] *= (percent + 1);
            }
            return values[0];
        } else{
            return -1;

        }
    }

    @Override
    public String toString() {
        return "Deposit Org" +'\n' +
                "min Time=" + minTime +'\n' +
                "max Limit=" + maxLimit +'\n' +
                "percent=" + percent +'\n' +
                "name='" + name + '\'' +'\n' +
                "address='" + address + '\'' +'\n';
    }
}
