package com.company.models;

import com.company.interfaces.OperationsWithMoney;

public class CreditOrg extends Org implements OperationsWithMoney {
    protected int maxLimit;
    protected float percent;

    public CreditOrg(String name, String address, int maxLimit, float percent) {
        super(name, address);
        this.maxLimit = maxLimit;
        this.percent = percent;
    }

    @Override
    public float iterateMoney(float[] values) {
        if (values.length < 3 || values[0] > maxLimit || values[0] < MIN_LIMIT || values[2] > 0) {
            return -1;
        } else {
            for (int i = 0; i < values[1]; i++) {
                values[0] *= (percent + 1);
            }
            return values[0];
        }
    }

    @Override
    public String toString() {
        return "Credit Org" + '\n' +
                "maxLimit=" + maxLimit + '\n' +
                "percent=" + percent + '\n' +
                "name='" + name + '\'' + '\n' +
                "address='" + address + '\'' + '\n';
    }
}
