package com.company.models;

import com.company.interfaces.Sent;

public class Sender extends Org implements Sent {
    float commission;

    public Sender(String name, String address, float commission) {
        super(name, address);
        this.commission = commission;
    }

    @Override
    public float sendAndGet(float pkg) {
        if(pkg >= MIN_PACKAGE) {
            return pkg * (1 - commission);
        } else{
            return -1;
        }
    }

    @Override
    public String toString() {
        return "Sender" +'\n' +
                "commission=" + commission +'\n' +
                "name='" + name + '\'' +'\n' +
                "address='" + address + '\'' +'\n';
    }
}
