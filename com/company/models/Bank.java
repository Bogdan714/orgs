package com.company.models;

import com.company.additional.Generator;
import com.company.interfaces.Changer;
import com.company.interfaces.OperationsWithMoney;
import com.company.interfaces.Sent;

import java.util.List;

public class Bank extends Org implements OperationsWithMoney, Sent, Changer {
    float commissionOfSending;
    float percentOnCredit;
    float percentOnDeposit;
    float commissionOfExchange;
    int maxLimitOfCredit;
    int maxLimitOfExchange;
    List<Currency> currencies;

    public Bank(String name, String address, float commissionOfSending, float percentOnCredit,
                float percentOnDeposit, float commissionOfExchange,
                int maxLimitOfCredit, int maxLimitOfExchange) {
        super(name, address);
        this.commissionOfSending = commissionOfSending;
        this.percentOnCredit = percentOnCredit;
        this.percentOnDeposit = percentOnDeposit;
        this.commissionOfExchange = commissionOfExchange;
        this.maxLimitOfCredit = maxLimitOfCredit;
        this.maxLimitOfExchange = maxLimitOfExchange;
        this.currencies = Generator.generateCurrencies(0);
    }

    @Override
    public float sendAndGet(float pkg) {
        if (pkg >= MIN_PACKAGE) {
            return pkg * (1 - commissionOfSending) - 5;
        } else {
            return -1;
        }
    }

    @Override
    public float iterateMoney(float[] values) {//{money, time, if more 0 then deposit}
        if (values.length > 2) {
            if (values[2] > 0 && values[0] > MIN_LIMIT && values[1] >= DEPOSIT_MIN_TIME) {
                for (int i = 0; i < values[1]; i++) {
                    values[0] *= (percentOnDeposit + 1);
                }
                return values[0];
            } else if (values[2] <= 0 && values[0] > MIN_LIMIT) {
                for (int i = 0; i < values[1]; i++) {
                    values[0] *= (percentOnCredit + 1);
                }
                return values[0];
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    public double change(float value, String from, String on) {
        int indexFrom = -1;
        int indexOn = -1;

        for (int i = 0; i < currencies.size(); i++) {
            if (from.equalsIgnoreCase(currencies.get(i).getName())) {
                indexFrom = i;
            }
            if (on.equalsIgnoreCase(currencies.get(i).getName())) {
                indexOn = i;
            }
        }
        if (indexFrom == -1 || indexOn == -1) {
            return -1;
        }
        currencies.indexOf(from);
        if (value < maxLimitOfExchange || value > MIN_LIMIT_OF_CHANGE) {
            return (value * (currencies.get(indexFrom).getCost() / currencies.get(indexOn).getCost())) * (1 - commissionOfExchange);
        }
        return -1;
    }

    @Override
    public boolean hasCurrency(String currency) {
        for (Currency currencyInOrg : currencies) {
            if (currencyInOrg.getName().equalsIgnoreCase(currency)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Bank" + '\n' +
                "commission Of Sending=" + commissionOfSending + '\n' +
                "percent On Credit=" + percentOnCredit + '\n' +
                "percent On Deposit=" + percentOnDeposit + '\n' +
                "commission Of Exchange=" + commissionOfExchange + '\n' +
                "maxLimit Of Credit=" + maxLimitOfCredit + '\n' +
                "maxLimit Of Exchange=" + maxLimitOfExchange + '\n' +
                "name='" + name + '\'' + '\n' +
                "address='" + address + '\'' + '\n';
    }
}
