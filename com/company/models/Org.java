package com.company.models;

public abstract class Org {
    protected String name;
    protected String address;

    public Org(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

}
