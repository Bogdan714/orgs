package com.company.models;

public class Currency {
    private String name;
    private double cost;

    public Currency(String name, double cost) {
        this.name = name;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Currency" +'\n' +
                "name='" + name + '\'' +'\n' +
                "cost=" + cost +'\n';
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }
}
