package com.company.models;

import com.company.interfaces.Changer;
import com.company.additional.Generator;

import java.util.List;

public class Exchanger extends Org implements Changer {
    private float commission;
    int maxLimit;
    List<Currency> currencies;

    public Exchanger(String name, String address, float commission, int maxLimit) {
        super(name, address);
        this.commission = commission;
        this.maxLimit = maxLimit;
        currencies = Generator.generateCurrencies(1);
    }

    public double change(float value, String from, String on) {
        int indexFrom = -1;
        int indexOn = -1;

        for (int i = 0; i < currencies.size(); i++) {
            if (from.equalsIgnoreCase(currencies.get(i).getName())) {
                indexFrom = i;
            }
            if (on.equalsIgnoreCase(currencies.get(i).getName())) {
                indexOn = i;
            }
        }
        if (indexFrom == -1 || indexOn == -1) {
            return -1;
        }
        currencies.indexOf(from);
        if (value < maxLimit || value > MIN_LIMIT_OF_CHANGE) {
            return (value * (currencies.get(indexFrom).getCost() / currencies.get(indexOn).getCost())) * (1 - commission);
        }
        return -1;
    }

    @Override
    public String toString() {
        return "Exchanger" +'\n' +
                "commission=" + commission +'\n' +
                "max Limit=" + maxLimit +'\n' +
                "currencies=" + currencies +'\n' +
                "name='" + name + '\'' +'\n' +
                "address='" + address + '\'';
    }

    @Override
    public boolean hasCurrency(String currency) {
        for (Currency currencyInOrg : currencies) {
            if (currencyInOrg.getName().equalsIgnoreCase(currency)) {
                return true;
            }
        }
        return false;
    }
}
